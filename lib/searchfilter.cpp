/*
    Copyright (c) 2017 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
#include "searchfilter.h"
#include "charactermodel.h"
void Aubergine::SearchFilter::setSearchString(const QString& string)
{
    if (m_searchString != string)
    {
        m_searchString = string;
        invalidateFilter();
    }
}

bool Aubergine::SearchFilter::lessThan(const QModelIndex& source_left, const QModelIndex& source_right) const
{
    return  source_left.row() < source_right.row();
}

bool Aubergine::SearchFilter::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    if (m_searchString.isEmpty())
    {
        return false;
    }
    auto sm = sourceModel();
    if (!sm) return false;
    return sm->index(source_row,0).data(Aubergine::CharacterModel::DescriptionRole).toString().contains(m_searchString,Qt::CaseInsensitive);
}
