/*
    Copyright (c) 2017 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
#include "parser.h"
#include <QIODevice>
#include <QDebug>
#include <QFile>

std::vector<Aubergine::Char> Aubergine::Parser::emojiList()
{
    static std::vector<Aubergine::Char> list = parseEmojiList();
    return list;
}

Aubergine::Char::Characters Aubergine::Parser::splitInt(int unicodechar)
{
    Char::Characters vec;
    if (QChar::requiresSurrogates(unicodechar))
    {
        QChar first = QChar::highSurrogate(unicodechar);
        QChar second = QChar::lowSurrogate(unicodechar);
        vec.push_back(first);
        vec.push_back(second);
    }
    else
    {
        vec.push_back(QChar(unicodechar));
    }
    return vec;
}


std::vector<Aubergine::Char> Aubergine::Parser::parseEmojiList()
{
    QFile f ("/usr/share/unicode/NamesList.txt");
    if (!f.exists()) {
        return {};
    }
    if (!f.open(QIODevice::ReadOnly)) {
        return {};
    }
    return Aubergine::Parser::parse(&f);
}

std::vector<Aubergine::Char> Aubergine::Parser::parse(QIODevice* dev) {
    Q_ASSERT(dev);
    std::vector<Aubergine::Char> vector;
    while (!dev->atEnd())
    {
        QByteArray arr = dev->readLine();
        if(arr.isEmpty()) continue;
        switch(arr.at(0)) {
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                break;
            default:
                continue;
        }
        QByteArrayList splitted = arr.split('\t');
        if (splitted.size() != 2) {
            continue;
        }
        bool ok;
        int number = splitted.at(0).toInt(&ok,16);
        if (!ok) {
            continue;
        }
        QByteArray description = splitted.at(1).trimmed();
        if (description == "<control>") {
            continue;
        }
        vector.emplace_back(Aubergine::Char{Aubergine::Parser::splitInt(number), QString::fromUtf8(splitted.at(1).trimmed())});
    }
    return std::move(vector);
}
