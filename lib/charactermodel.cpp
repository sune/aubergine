/*
    Copyright (c) 2017 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
#include "charactermodel.h"

Aubergine::CharacterModel::CharacterModel(std::vector<Aubergine::Char>&& data) : m_data(data)
{
}

int Aubergine::CharacterModel::rowCount(const QModelIndex& parent) const
{
    return int(m_data.size());
}

QVariant Aubergine::CharacterModel::data(const QModelIndex& idx, int role) const
{
    if (!idx.isValid())
        return QVariant();
    switch (role) {
    case Qt::DisplayRole:
    case DescriptionRole:
        return m_data[idx.row()].description;
    case CharacterRole:
    {
        auto& elm = m_data[idx.row()];
        QString str;
        str.reserve(elm.character.size());
        for(const auto& c : qAsConst(elm.character))
           str.push_back(c);
        return str;
    }
    default:
        break;
    }
    return QVariant();
}

QHash<int, QByteArray> Aubergine::CharacterModel::roleNames() const
{
    auto roles = QAbstractListModel::roleNames();
    roles.insert(DescriptionRole,"description");
    roles.insert(CharacterRole, "character");
    return roles;
}

void Aubergine::CharacterModel::updateData(std::vector<Aubergine::Char>&& data)
{
    beginResetModel();
    m_data = data;
    endResetModel();
}
