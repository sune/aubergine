/*
    Copyright (c) 2017 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
#include <QTest>
#include <QVector>
#include <QBuffer>
#include <QFile>
#include "parser.h"

class TestParser : public QObject
{
    Q_OBJECT
    private Q_SLOTS:

         void testSplitInt();
         void testSplitInt_data();
         void testSimpleParse();
         void testSimpleParse_data();
         void testFullFile();

};

void TestParser::testSplitInt()
{
    QFETCH(QString, result);
    QFETCH(int, codepoint);

    auto vec = Aubergine::Parser::splitInt(codepoint);
    for(auto c : vec)
    {
        QVERIFY(!c.isNull());
    }
    QString str(vec);
    QCOMPARE(str, result);
}
void TestParser::testSplitInt_data()
{
    QTest::addColumn<QString>("result");
    QTest::addColumn<int>("codepoint");
    QTest::newRow("catface") << QString( "\U0001F431") << 0x1f431;
    QTest::newRow("aubergine") << QString("\U0001F346") << 0x1F346;
    QTest::newRow("a") << QString("a") << (int)'a';
    QTest::newRow("A") << QString("A") << (int)'A';
}


void TestParser::testFullFile()
{
    QFile f ("/usr/share/unicode/NamesList.txt");
    if (!f.exists()) {
        QSKIP("unicode data file not found");
    }
    QVERIFY(f.open(QIODevice::ReadOnly));
    auto parsed = Aubergine::Parser::parse(&f);
    QVERIFY(30000 < int(parsed.size()));
}

void TestParser::testSimpleParse()
{
    QFETCH(QByteArray, data);
    QFETCH(int, count);
    QFETCH(QVector<Aubergine::Char::Characters>, parsedData);
    QFETCH(QVector<QString>, parsedDescriptions);
    QCOMPARE(count, parsedData.size());
    QCOMPARE(count, parsedDescriptions.size());

    QBuffer b(&data);
    QVERIFY(b.open(QIODevice::ReadOnly));
    auto parsed = Aubergine::Parser::parse(&b);
    QCOMPARE(count, int(parsed.size()));
    for (int i = 0 ; i < count ; i++) {
        QCOMPARE(parsedData.at(i), parsed[i].character);
        QCOMPARE(parsedDescriptions.at(i), parsed[i].description);
    }

}

void TestParser::testSimpleParse_data()
{
    QTest::addColumn<QByteArray>("data");
    QTest::addColumn<int>("count");
    QTest::addColumn<QVector<Aubergine::Char::Characters>>("parsedData");
    QTest::addColumn<QVector<QString>>("parsedDescriptions");

    QTest::newRow("empty") << QByteArray("") << 0 << QVector<Aubergine::Char::Characters>{} << QVector<QString>{};
    QTest::newRow("controlchar") << QByteArray("0001\t<control>\n\t= START OF HEADING") << 0 << QVector<Aubergine::Char::Characters>{} << QVector<QString>{};
    QTest::newRow("header") << QByteArray("@               Variant letterforms") << 0 << QVector<Aubergine::Char::Characters>{} << QVector<QString>{};
    QTest::newRow("aubergine") << QByteArray("1F346\tAUBERGINE") << 1 << QVector<Aubergine::Char::Characters>{Aubergine::Parser::splitInt(0x1f346)} << QVector<QString>{"AUBERGINE"};
    QTest::newRow("kissingcat") << QByteArray("1F63D\tKISSING CAT FACE WITH CLOSED EYES") << 1 << QVector<Aubergine::Char::Characters>{Aubergine::Parser::splitInt(0x1f63d)} << QVector<QString>{"KISSING CAT FACE WITH CLOSED EYES"};
    QTest::newRow("ampersand") << QByteArray("0026\tAMPERSAND\n\tx (tironian sign et - 204A)\n\tx (turned ampersand - 214B)\n\tx (heavy ampersand ornament - 1F674)") << 1 << QVector<Aubergine::Char::Characters>{Aubergine::Parser::splitInt(0x26)} <<  QVector<QString>{"AMPERSAND"};
    QTest::newRow("twolettersandaheader") << QByteArray("03EE\tCOPTIC CAPITAL LETTER DEI\n03EF\tCOPTIC SMALL LETTER DEI\n@               Variant letterforms") << 2 << QVector<Aubergine::Char::Characters>{Aubergine::Parser::splitInt(0x03ee), Aubergine::Parser::splitInt(0x3ef)} << QVector<QString>{"COPTIC CAPITAL LETTER DEI","COPTIC SMALL LETTER DEI"};
}

QTEST_GUILESS_MAIN(TestParser)

#include "testparser.moc"
