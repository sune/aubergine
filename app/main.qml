/*
    Copyright (c) 2017 Sune Vuorela <sune@vuorela.dk>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation
    files (the "Software"), to deal in the Software without
    restriction, including without limitation the rights to use,
    copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following
    conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THE SOFTWARE.
*/
import QtQuick 2.5
import QtQuick.Controls 2.2
import Aubergine 1.0

ApplicationWindow {
    height: 200
    width: 200
    visible: true
    Item {
        anchors.margins: 5
        anchors.fill: parent;
        TextField {
            id: input
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            text: "cat face"
            onTextChanged: searchModel.setSearchString(text);
            Component.onCompleted: {searchModel.setSearchString(text); forceActiveFocus();}
        }

        GridView {
            Clipboard
            {
                id: clippy
            }
            clip: true
            anchors.top: input.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            model: SearchFilter {
                id: searchModel
                sourceModel: emojiModel
            }
            delegate: Button {
                font.pointSize: 30
                hoverEnabled: true
                onClicked: clippy.text = character
                text: character
                ToolTip.visible: hovered
                ToolTip.text: description
            }
        }
    }
}
